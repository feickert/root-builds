# root-builds

To build versions of root for which there are no docker images available on [https://hub.docker.com/u/rootproject](https://hub.docker.com/u/rootproject).

# Local Building
You can build this using standard Docker building methods
```
docker build -t <TAGNAME> .
```