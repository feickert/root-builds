FROM cern/slc6-base:latest
RUN yum install -y \
      zip \
      tar \
      wget \
      which \
      libXpm-devel \
      libXft-devel \
      libXext-devel \
      gcc-c++.x86_64 \
      libX11-devel \
      glibc-devel.i686 \
      glibc-devel
RUN wget https://github.com/root-project/root/archive/v5-34-36.tar.gz && \
    tar -xzvf v5-34-36.tar.gz && \
    cd root-5-34-36/ && \
    ./configure --help && \
    ./configure \
      linuxx8664gcc \
      --prefix=/usr/local \
      --disable-x11 \
      --enable-xml \
      --with-x11-libdir=/usr/lib64 \
      --with-xpm-libdir=/usr/lib64/ \
      --with-xft-libdir=/usr/lib64/ \
      --with-xext-libdir=/usr/lib64 && \
    make -j$(($(nproc)-2)) && \
    make install && \
    echo "source /usr/local/bin/thisroot.sh" >> /root/.bashrc && \
    echo "alias 'root=root.exe'" >> /root/.bashrc
ENV LD_LIBRARY_PATH=/usr/local/lib:$LD_LIBRARY_PATH
ENV ROOTSYS=/usr/local
WORKDIR /root